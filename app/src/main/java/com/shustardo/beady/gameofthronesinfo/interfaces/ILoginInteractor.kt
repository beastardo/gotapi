package com.shustardo.beady.gameofthronesinfo.interfaces

interface ILoginInteractor {
    fun doLogin(mail:String, pass:String)
}
