package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotHouseModel
import io.reactivex.Observable

interface ICharacterFragmentInteractor {
    fun loadData(pageId: String)
    fun recoverHouse(url: String): Observable<GotHouseModel>
}