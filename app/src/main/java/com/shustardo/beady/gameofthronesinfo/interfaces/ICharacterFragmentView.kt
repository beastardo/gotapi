package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotCharacterPreviewModel

interface ICharacterFragmentView {
    fun inflateRecyclerView(bookPreviewModelList: List<GotCharacterPreviewModel>)
    fun refresh()
}