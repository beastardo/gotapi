package com.shustardo.beady.gameofthronesinfo.models

import com.google.gson.annotations.SerializedName

data class GotBookModel(
	@field:SerializedName("povCharacters")
	val povCharacters: List<String?>? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("characters")
	val characters: List<String?>? = null,

	@field:SerializedName("numberOfPages")
	val numberOfPages: Int? = null,

	@field:SerializedName("isbn")
	val isbn: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("publisher")
	val publisher: String? = null,

	@field:SerializedName("mediaType")
	val mediaType: String? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("released")
	val released: String? = null,

	@field:SerializedName("authors")
	val authors: List<String?>? = null
)