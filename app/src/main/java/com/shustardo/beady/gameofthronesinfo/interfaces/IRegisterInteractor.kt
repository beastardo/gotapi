package com.shustardo.beady.gameofthronesinfo.interfaces

interface IRegisterInteractor {
    fun doRegistration(email: String, password: String)
}