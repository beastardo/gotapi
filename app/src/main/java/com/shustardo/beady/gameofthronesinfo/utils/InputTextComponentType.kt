package com.shustardo.beady.gameofthronesinfo.utils

enum class InputTextComponentType {
    EMAIL_INPUT,
    PASS_INPUT,
    PHONE_INPUT,
    DEFAULT
}