package com.shustardo.beady.gameofthronesinfo.interfaces

import com.google.firebase.auth.FirebaseUser

interface IRegisterPresenter {
    fun doRegistration(email: String, password: String)
    fun correctLogin(firebaseUser: FirebaseUser)
    fun showError(error: String)
}