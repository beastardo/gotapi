package com.shustardo.beady.gameofthronesinfo.presenters

import com.shustardo.beady.gameofthronesinfo.interactors.BookFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IBookFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IBookFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.IBookFragmentView
import com.shustardo.beady.gameofthronesinfo.interfaces.IMainActivityView
import com.shustardo.beady.gameofthronesinfo.models.GotBookPreviewModel

class BookFragmentPresenter(val viewMainActivity: IMainActivityView, val viewFragment: IBookFragmentView) : IBookFragmentPresenter {
    private val interactor: IBookFragmentInteractor = BookFragmentInteractor(this)

    override fun loadData(pageId: String) {
        viewMainActivity.showProgress(true)
        interactor.loadData(pageId)
    }

    override fun returnData(bookPreviewModelList: List<GotBookPreviewModel>) {
        viewMainActivity.showProgress(false)
        viewFragment.inflateRecyclerView(bookPreviewModelList)
    }
}