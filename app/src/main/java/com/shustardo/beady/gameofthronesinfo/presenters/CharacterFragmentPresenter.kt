package com.shustardo.beady.gameofthronesinfo.presenters

import com.shustardo.beady.gameofthronesinfo.interactors.CharacterFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.ICharacterFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.ICharacterFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.ICharacterFragmentView
import com.shustardo.beady.gameofthronesinfo.interfaces.IMainActivityView
import com.shustardo.beady.gameofthronesinfo.models.GotCharacterPreviewModel

class CharacterFragmentPresenter(val viewMainActivity: IMainActivityView, val viewFragment: ICharacterFragmentView) : ICharacterFragmentPresenter {
    private val interactor: ICharacterFragmentInteractor = CharacterFragmentInteractor(this)

    override fun loadData(pageId: String) {
        viewMainActivity.showProgress(true)
        interactor.loadData(pageId)
    }

    override fun returnData(characterPreviewModelList: List<GotCharacterPreviewModel>) {
        viewMainActivity.showProgress(false)

        //TODO: no borrar, util para obtener los nombres de las casas a las que pertenece un personaje
//        for (character in characterPreviewModelList) {
//            if (!character.alleginances!!.isEmpty()) {
//                for (i in character.alleginances!!.indices) {
//                    interactor.recoverHouse(character.alleginances!![i])
//                            .subscribeOn(Schedulers.newThread())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(
//                                    { result ->
//                                        character.alleginances!![i] =
//                                                FromApiModelConverterUtils().importToHousePreviewModel(result).name
//
//                                        viewFragment.refresh()
//                                    },
//                                    {
//                                        error ->
//                                        Logger.e("GoT Test" + error.message)
//                                    }
//                            )
//                }
//
//            }
//        }

        viewFragment.inflateRecyclerView(characterPreviewModelList)
    }
}