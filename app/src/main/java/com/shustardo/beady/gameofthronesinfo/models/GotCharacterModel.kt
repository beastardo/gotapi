package com.shustardo.beady.gameofthronesinfo.models

import com.google.gson.annotations.SerializedName

data class GotCharacterModel(
	@field:SerializedName("aliases")
	val aliases: List<String?>? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("born")
	val born: String? = null,

	@field:SerializedName("father")
	val father: String? = null,

	@field:SerializedName("playedBy")
	val playedBy: List<String?>? = null,

	@field:SerializedName("died")
	val died: String? = null,

	@field:SerializedName("titles")
	val titles: List<String?>? = null,

	@field:SerializedName("allegiances")
	var allegiances: MutableList<String>? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("spouse")
	val spouse: String? = null,

	@field:SerializedName("mother")
	val mother: String? = null,

	@field:SerializedName("books")
	val books: List<String?>? = null,

	@field:SerializedName("culture")
	val culture: String? = null,

	@field:SerializedName("tvSeries")
	val tvSeries: List<String?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("povBooks")
	val povBooks: List<Any?>? = null
)