package com.shustardo.beady.gameofthronesinfo.presenters

import com.orhanobut.logger.Logger
import com.shustardo.beady.gameofthronesinfo.interactors.HouseFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IHouseFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IHouseFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.IHouseFragmentView
import com.shustardo.beady.gameofthronesinfo.interfaces.IMainActivityView
import com.shustardo.beady.gameofthronesinfo.models.GotHousePreviewModel
import com.shustardo.beady.gameofthronesinfo.utils.FromApiModelConverterUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HouseFragmentPresenter(val mainActivity: IMainActivityView, val view: IHouseFragmentView) : IHouseFragmentPresenter {
    private val interactor: IHouseFragmentInteractor = HouseFragmentInteractor(this)

    override fun loadData(pageID: String) {
        mainActivity.showProgress(true)
        interactor.loadData(pageID)
    }

    override fun returnData(housePreviewModelList: List<GotHousePreviewModel>) {
        mainActivity.showProgress(false)

        for (house in housePreviewModelList) {
            if (house.founder.isEmpty()) {
                house.founder = "Unavailable"
                if (house.founded.isEmpty()) {
                    house.founded = "Unavailable"
                }
            } else {

                interactor.recoverCharacter(house.founder)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result ->
                                    house.founder = FromApiModelConverterUtils().importToCharacterPreviewModel(result).name
                                },
                                { error ->
                                    Logger.e("GoT Test" + error.message)
                                }
                        )
            }
        }
        view.inflateRecyclerView(housePreviewModelList)
    }
}