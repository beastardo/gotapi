package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotBookModel
import com.shustardo.beady.gameofthronesinfo.models.GotCharacterModel
import com.shustardo.beady.gameofthronesinfo.models.GotHouseModel
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.Path
import retrofit2.http.Query

interface IGotService {
    @GET(ConstantUtils.API_ENDPOINT_BOOKS)
    fun searchBookList(): Observable<List<GotBookModel>>

    @GET(ConstantUtils.API_ENDPOINT_CHARS)
    fun searchCharList(): Observable<List<GotCharacterModel>>

    @GET(ConstantUtils.API_ENDPOINT_HOUSES)
    fun searchHouseList(): Observable<List<GotHouseModel>>

    @GET(ConstantUtils.API_ENDPOINT_BOOKS + "/{id}")
    fun searchBook(@Path("id") bookId: String): Observable<GotBookModel>

    @GET(ConstantUtils.API_ENDPOINT_CHARS + "/{id}")
    fun searchCharacter(@Path("id") charId: String): Observable<GotCharacterModel>

    @GET(ConstantUtils.API_ENDPOINT_HOUSES + "/{id}")
    fun searchHouse(@Path("id") houseId: String): Observable<GotHouseModel>

    @GET(ConstantUtils.API_ENDPOINT_BOOKS + ConstantUtils.API_PAGINATED_PAGE_SIZE + ConstantUtils.API_PAGINATED_DEFAULT_RESULTS)
    fun searchBooksPaginated(@Query("page") pageId: String): Observable<List<GotBookModel>>

    @GET(ConstantUtils.API_ENDPOINT_CHARS + ConstantUtils.API_PAGINATED_PAGE_SIZE + ConstantUtils.API_PAGINATED_DEFAULT_RESULTS)
    fun searchCharactersPaginated(@Query("page") pageId: String): Observable<List<GotCharacterModel>>

    @GET(ConstantUtils.API_ENDPOINT_HOUSES + ConstantUtils.API_PAGINATED_PAGE_SIZE + ConstantUtils.API_PAGINATED_DEFAULT_RESULTS)
    fun searchHousesPaginated(@Query("page") pageId: String): Observable<List<GotHouseModel>>

    companion object Factory {
        fun create(): IGotService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ConstantUtils.API_BASE_URL)
                    .build()

            return retrofit.create(IGotService::class.java)
        }
    }
}