package com.shustardo.beady.gameofthronesinfo.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.adapters.HouseViewAdapter
import com.shustardo.beady.gameofthronesinfo.components.listeners.EndlessRecyclerViewScrollListener
import com.shustardo.beady.gameofthronesinfo.interfaces.IHouseFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.IHouseFragmentView
import com.shustardo.beady.gameofthronesinfo.interfaces.IMainActivityView
import com.shustardo.beady.gameofthronesinfo.models.GotHousePreviewModel
import com.shustardo.beady.gameofthronesinfo.presenters.HouseFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils

class HouseFragment : Fragment(), IHouseFragmentView {
    private lateinit var mRecycler: RecyclerView
    private lateinit var mViewMainActivity: IMainActivityView
    private lateinit var mPresenter: IHouseFragmentPresenter
    private lateinit var dataList: ArrayList<GotHousePreviewModel>
    private lateinit var adapter: HouseViewAdapter
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_house, container, false)
        val layoutManager = LinearLayoutManager(context)
        mRecycler = view.findViewById(R.id.house_pre_recyclerview) as RecyclerView
        dataList = ArrayList<GotHousePreviewModel>()
        adapter = HouseViewAdapter(dataList)

        mPresenter = HouseFragmentPresenter(mViewMainActivity, this)

        mRecycler.adapter = HouseViewAdapter(dataList)
        mRecycler.layoutManager = layoutManager

        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager, ConstantUtils.ENDLESS_CHAR_THRESHOLD) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                loadNextDataFromApi(page)
            }
        }
        mRecycler.addOnScrollListener(scrollListener)

        mPresenter.loadData(ConstantUtils.API_PAGINATED_FIRST_PAGE)
        return view
    }

    companion object {
        fun newInstance(view: IMainActivityView): HouseFragment {
            val fragment = HouseFragment()
            fragment.setParams(view)
            return fragment
        }
    }

    private fun loadNextDataFromApi(page: Int) {
        mPresenter.loadData(page.toString())
    }

    override fun inflateRecyclerView(characterPreviewModelList: List<GotHousePreviewModel>) {
        dataList.addAll(characterPreviewModelList)
        mRecycler.adapter.notifyDataSetChanged()
    }

    fun setParams(view: IMainActivityView) {
        mViewMainActivity = view
    }
}
