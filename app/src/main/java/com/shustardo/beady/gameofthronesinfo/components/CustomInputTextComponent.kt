package com.shustardo.beady.gameofthronesinfo.components

import android.content.Context
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.text.method.TransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils
import com.shustardo.beady.gameofthronesinfo.utils.InputTextComponentType

class CustomInputTextComponent : LinearLayout {
    private val layoutId = R.layout.component_login_input_layout
    private lateinit var editText: EditText
    private lateinit var imageView: ImageView

    constructor(context: Context?) : super(context) {
        initComponent(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initComponent(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initComponent(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initComponent(context)
    }

    private fun initComponent(context: Context?) {
        initView()
    }

    private fun initView() {
        LayoutInflater.from(context).inflate(layoutId, this, true)
        editText = findViewById<EditText>(R.id.custom_input_edittext)
        imageView = findViewById<ImageView>(R.id.custom_input_imageview)
    }

    fun initType(type: InputTextComponentType) {
        when (type) {
            //Case email input
            InputTextComponentType.EMAIL_INPUT -> {
                setUpComponentType(false, R.drawable.ic_person_black_24dp, resources.getDimension(R.dimen.md_EditText_drawablePadding).toInt(),
                        resources.getString(R.string.login_user_hint), InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
            }

            //Case pass input
            InputTextComponentType.PASS_INPUT -> {
                setUpComponentType(false, R.drawable.ic_lock_black_24dp, resources.getDimension(R.dimen.md_EditText_drawablePadding).toInt(),
                        resources.getString(R.string.login_pass_hint), InputType.TYPE_CLASS_TEXT)

                //Specific line for setting the EditText in "password mode"
                //setInputType() doesn't work for password input type
                editText.transformationMethod = PasswordTransformationMethod.getInstance()
            }

            //Default
            else -> {
                setUpComponentType(true, ConstantUtils.ZERO_VALUE, ConstantUtils.ZERO_VALUE,
                        ConstantUtils.EMPTY_STRING, InputType.TYPE_CLASS_TEXT)
            }
        }
    }

    private fun setUpComponentType(isDefault: Boolean, drawableStart: Int, drawablePadding: Int, hint: String, inputType: Int) {
        editText.setCompoundDrawablesWithIntrinsicBounds(drawableStart, ConstantUtils.ZERO_VALUE,
                ConstantUtils.ZERO_VALUE, ConstantUtils.ZERO_VALUE)
        editText.compoundDrawablePadding = drawablePadding
        editText.hint = hint
        editText.inputType = inputType
        if (isDefault) {
            imageView.visibility = View.GONE
        } else {
            imageView.setOnClickListener {
                editText.setText(ConstantUtils.EMPTY_STRING)
            }
        }
    }

    fun getText(): String {
        return editText.text.toString()
    }
}