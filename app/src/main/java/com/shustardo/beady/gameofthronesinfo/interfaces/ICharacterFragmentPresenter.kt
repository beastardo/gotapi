package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotCharacterPreviewModel

interface ICharacterFragmentPresenter {
    fun loadData(pageId: String)
    fun returnData(bookPreviewModelList: List<GotCharacterPreviewModel>)
}