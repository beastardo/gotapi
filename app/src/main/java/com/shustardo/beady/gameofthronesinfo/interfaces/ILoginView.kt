package com.shustardo.beady.gameofthronesinfo.interfaces

import com.google.firebase.auth.FirebaseUser

interface ILoginView {
    fun doLoginOK(user: FirebaseUser)
    fun showError(msg: String)
}

