package com.shustardo.beady.gameofthronesinfo.presenters

import com.google.firebase.auth.FirebaseUser
import com.shustardo.beady.gameofthronesinfo.interactors.LoginInteractorImpl
import com.shustardo.beady.gameofthronesinfo.interfaces.ILoginPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.ILoginView

class LoginPresenterImpl(var mView: ILoginView) : ILoginPresenter {
    private var mInteractor: LoginInteractorImpl = LoginInteractorImpl(this)

    override fun doLogin(mail: String, pass: String) {
        mInteractor.doLogin(mail, pass)
    }

    override fun doLoginOK(user: FirebaseUser) {
        mView.doLoginOK(user)
    }

    override fun showError(msg: String) {
        mView.showError(msg)
    }
}