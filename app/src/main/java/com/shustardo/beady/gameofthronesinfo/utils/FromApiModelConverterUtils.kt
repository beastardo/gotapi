package com.shustardo.beady.gameofthronesinfo.utils

import com.shustardo.beady.gameofthronesinfo.models.*

class FromApiModelConverterUtils {
    //BOOKS
    fun importToBookPreviewModel(book: GotBookModel): GotBookPreviewModel {
        return GotBookPreviewModel(book.name!!, book.isbn!!, book.url!!)
    }

    fun importToListBookPreviewModel(bookList: List<GotBookModel>): List<GotBookPreviewModel> {
        var listBookPreview: ArrayList<GotBookPreviewModel> = ArrayList()
        bookList.mapTo(listBookPreview) { importToBookPreviewModel(it) }
        return listBookPreview
    }

    //CHARACTERS
    fun importToCharacterPreviewModel(character: GotCharacterModel): GotCharacterPreviewModel {
        val characterPreview: GotCharacterPreviewModel = GotCharacterPreviewModel(character.name!!,
                character.aliases, character.gender!!, character.url!!, character.allegiances)
        return characterPreview
    }

    fun importToListCharacterPreviewModel(characterList: List<GotCharacterModel>): List<GotCharacterPreviewModel> {
        var listCharacterPreview: ArrayList<GotCharacterPreviewModel> = ArrayList()
        characterList.mapTo(listCharacterPreview) { importToCharacterPreviewModel(it) }
        return listCharacterPreview
    }

    //HOUSES
    fun importToHousePreviewModel(house: GotHouseModel): GotHousePreviewModel {
        val housePreview: GotHousePreviewModel = GotHousePreviewModel(house.name!!, house.region!!, house.founder!!,
                house.founded!!, house.url!!)
        return housePreview
    }

    fun importToListHousePreviewModel(houseList: List<GotHouseModel>): List<GotHousePreviewModel> {
        var listHousePreview: ArrayList<GotHousePreviewModel> = ArrayList()
        houseList.mapTo(listHousePreview) { importToHousePreviewModel(it) }
        return listHousePreview
    }
}