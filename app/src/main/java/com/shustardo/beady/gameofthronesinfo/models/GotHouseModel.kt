package com.shustardo.beady.gameofthronesinfo.models

import com.google.gson.annotations.SerializedName

data class GotHouseModel(
	@field:SerializedName("swornMembers")
	val swornMembers: List<String?>? = null,

	@field:SerializedName("founder")
	val founder: String? = null,

	@field:SerializedName("cadetBranches")
	val cadetBranches: List<Any?>? = null,

	@field:SerializedName("words")
	val words: String? = null,

	@field:SerializedName("founded")
	val founded: String? = null,

	@field:SerializedName("titles")
	val titles: List<String?>? = null,

	@field:SerializedName("seats")
	val seats: List<String?>? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("coatOfArms")
	val coatOfArms: String? = null,

	@field:SerializedName("diedOut")
	val diedOut: String? = null,

	@field:SerializedName("heir")
	val heir: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("region")
	val region: String? = null,

	@field:SerializedName("currentLord")
	val currentLord: String? = null,

	@field:SerializedName("ancestralWeapons")
	val ancestralWeapons: List<Any?>? = null,

	@field:SerializedName("overlord")
	val overlord: String? = null
)