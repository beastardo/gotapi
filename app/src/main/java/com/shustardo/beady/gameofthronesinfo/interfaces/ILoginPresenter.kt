package com.shustardo.beady.gameofthronesinfo.interfaces

import com.google.firebase.auth.FirebaseUser

interface ILoginPresenter {
    fun doLogin(mail:String, pass:String)
    fun doLoginOK(user: FirebaseUser)
    fun showError(msg: String)
}
