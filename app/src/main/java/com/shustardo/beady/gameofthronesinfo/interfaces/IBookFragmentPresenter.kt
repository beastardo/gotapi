package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotBookPreviewModel

interface IBookFragmentPresenter {
    fun loadData(toString: String)
    fun returnData(bookPreviewModelList: List<GotBookPreviewModel>)
}