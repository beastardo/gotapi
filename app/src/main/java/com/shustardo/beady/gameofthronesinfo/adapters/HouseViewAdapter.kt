package com.shustardo.beady.gameofthronesinfo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.models.GotHousePreviewModel

class HouseViewAdapter(val booksList: ArrayList<GotHousePreviewModel>) : RecyclerView.Adapter<HouseViewAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val house: GotHousePreviewModel = booksList[position]
        holder?.house_name?.text = house.name
        holder?.house_location?.text = house.region
        holder?.house_founded?.text = "Founded: ${house.founded}"
        holder?.house_founder?.text = "By: ${house.founder}"
    }

    override fun getItemCount(): Int {
        return booksList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.house_preview, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val house_name = itemView.findViewById(R.id.house_pre_name) as TextView
        val house_location = itemView.findViewById(R.id.house_pre_location) as TextView
        val house_founded = itemView.findViewById(R.id.house_pre_founded) as TextView
        val house_founder = itemView.findViewById(R.id.house_pre_founder) as TextView
    }
}