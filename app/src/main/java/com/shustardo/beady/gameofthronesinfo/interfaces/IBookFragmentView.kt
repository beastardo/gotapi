package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotBookPreviewModel

interface IBookFragmentView {
    fun inflateRecyclerView(bookPreviewModelList:List<GotBookPreviewModel>)
}