package com.shustardo.beady.gameofthronesinfo.api

import com.shustardo.beady.gameofthronesinfo.interfaces.IGotService

object GotRepositoryProvider {
    fun provideGotRepository(): GotRepository {
        return GotRepository(IGotService.create())
    }
}