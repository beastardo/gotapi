package com.shustardo.beady.gameofthronesinfo.utils

class GotHouseMapUtils {
    private var houseMap: MutableMap<String, String> = HashMap()

    fun addHouse(key: String, name: String) {
        if (!checkHouseInMap(key)) {
            houseMap.put(key, name)
        }
    }

    fun getHouseName(key: String): String {
        if (checkHouseInMap(key)) {
            return houseMap.getValue(key)
        }
        return ConstantUtils.EMPTY_STRING
    }

    fun checkHouseInMap(key: String): Boolean {
        if (houseMap.containsKey(key)) {
            return true
        }
        return false
    }
}