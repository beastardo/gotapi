package com.shustardo.beady.gameofthronesinfo.interactors

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.shustardo.beady.gameofthronesinfo.extensions.validateEmail
import com.shustardo.beady.gameofthronesinfo.extensions.validatePassword
import com.shustardo.beady.gameofthronesinfo.interfaces.ILoginInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.ILoginPresenter

class LoginInteractorImpl(var mPresenter: ILoginPresenter) : ILoginInteractor {
    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var user: FirebaseUser

    override fun doLogin(mail: String, pass: String) {
        if (mail.validateEmail() && pass.validatePassword()) {
            mAuth.signInWithEmailAndPassword(mail, pass).addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    user = mAuth.currentUser!!
                    mPresenter.doLoginOK(user)
                } else {
                    mPresenter.showError("LOGIN KO")
                }
            }
        } else {
            mPresenter.showError("LOGIN KO")
        }
    }
}
