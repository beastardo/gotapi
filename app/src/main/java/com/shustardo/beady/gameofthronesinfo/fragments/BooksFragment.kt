package com.shustardo.beady.gameofthronesinfo.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.adapters.BookGridViewAdapter
import com.shustardo.beady.gameofthronesinfo.components.listeners.EndlessRecyclerViewScrollListener
import com.shustardo.beady.gameofthronesinfo.interfaces.IBookFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.IBookFragmentView
import com.shustardo.beady.gameofthronesinfo.interfaces.IMainActivityView
import com.shustardo.beady.gameofthronesinfo.models.GotBookPreviewModel
import com.shustardo.beady.gameofthronesinfo.presenters.BookFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils

class BooksFragment : Fragment(), IBookFragmentView {
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var presenter: IBookFragmentPresenter
    private lateinit var mViewMainActivity: IMainActivityView
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener
    private lateinit var dataList: ArrayList<GotBookPreviewModel>
    private lateinit var adapter: BookGridViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_books, container, false)
        //TODO: considerar otros layouts como StaggeredGrid o LinearLayout
        val layoutManager = GridLayoutManager(context, 2)

        mRecyclerView = view.findViewById(R.id.book_recyclergridview) as RecyclerView

        presenter = BookFragmentPresenter(mViewMainActivity, this)
        dataList = ArrayList<GotBookPreviewModel>()
        adapter = BookGridViewAdapter(dataList, this)

        mRecyclerView.layoutManager = layoutManager
        mRecyclerView.adapter = adapter

        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager, ConstantUtils.ENDLESS_CHAR_THRESHOLD) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                loadNextDataFromApi(page)
            }
        }
        mRecyclerView.addOnScrollListener(scrollListener)
        presenter.loadData(ConstantUtils.API_PAGINATED_FIRST_PAGE)

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    private fun loadNextDataFromApi(page: Int) {
        presenter.loadData(page.toString())
    }

    override fun inflateRecyclerView(characterPreviewModelList: List<GotBookPreviewModel>) {
        dataList.addAll(characterPreviewModelList)
        mRecyclerView.adapter.notifyDataSetChanged()
    }

    fun setArguments(viewMainActivity: IMainActivityView) {
        mViewMainActivity = viewMainActivity
    }
}
