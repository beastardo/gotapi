package com.shustardo.beady.gameofthronesinfo.models

data class GotCharacterPreviewModel(val name: String, val alias: List<String?>?, val gender: String, val url: String, var alleginances: MutableList<String>?)