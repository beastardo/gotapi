package com.shustardo.beady.gameofthronesinfo.presenters

import com.google.firebase.auth.FirebaseUser
import com.shustardo.beady.gameofthronesinfo.interactors.RegisterInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IRegisterInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IRegisterPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.IRegisterView

class RegisterPresenter(var view: IRegisterView) : IRegisterPresenter {
    private var interactor: IRegisterInteractor = RegisterInteractor(this)

    override fun doRegistration(email: String, password: String) {
        view.showProgress(true)
        interactor.doRegistration(email, password)
    }

    override fun correctLogin(firebaseUser: FirebaseUser) {
        view.showProgress(false)
        view.correctRegistration(firebaseUser)
    }

    override fun showError(error: String) {
        view.showProgress(false)
        view.showError(error)
    }
}