package com.shustardo.beady.gameofthronesinfo.interactors

import android.util.Log
import com.shustardo.beady.gameofthronesinfo.api.GotRepository
import com.shustardo.beady.gameofthronesinfo.api.GotRepositoryProvider
import com.shustardo.beady.gameofthronesinfo.extensions.lastValue
import com.shustardo.beady.gameofthronesinfo.interfaces.ICharacterFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.ICharacterFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.models.GotHouseModel
import com.shustardo.beady.gameofthronesinfo.utils.FromApiModelConverterUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CharacterFragmentInteractor(var characterFragmentPresenter: ICharacterFragmentPresenter) : ICharacterFragmentInteractor {
    private val provider: GotRepository = GotRepositoryProvider.provideGotRepository()

    override fun loadData(pageId: String) {
        provider.getPaginatedChars(pageId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            characterFragmentPresenter.returnData(FromApiModelConverterUtils().importToListCharacterPreviewModel(result))
                        },
                        { error ->
                            Log.e("GoT Test", error.message)
                        }
                )
    }

    override fun recoverHouse(url: String): Observable<GotHouseModel> {
        return provider.getHouse(url.lastValue())
    }
}