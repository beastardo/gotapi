package com.shustardo.beady.gameofthronesinfo.interfaces

import com.google.firebase.auth.FirebaseUser

interface IRegisterView {
    fun showError(error: String)
    fun correctRegistration(firebaseUser: FirebaseUser)
    fun showProgress(show: Boolean)
}