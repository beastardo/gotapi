package com.shustardo.beady.gameofthronesinfo.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseUser
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.components.CustomInputTextComponent
import com.shustardo.beady.gameofthronesinfo.interfaces.ILoginView
import com.shustardo.beady.gameofthronesinfo.presenters.LoginPresenterImpl
import com.shustardo.beady.gameofthronesinfo.utils.InputTextComponentType

class LoginActivity : AppCompatActivity(), ILoginView {
    private lateinit var mailInput: CustomInputTextComponent
    private lateinit var passInput: CustomInputTextComponent
    private lateinit var loginBtn: Button
    private lateinit var regText: TextView

    lateinit var mPresenter: LoginPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_registration_layout)

        mPresenter = LoginPresenterImpl(this)

        init()
    }

    private fun init() {
        mailInput = findViewById<CustomInputTextComponent>(R.id.input_user_mail)
        passInput = findViewById<CustomInputTextComponent>(R.id.input_user_pass)
        loginBtn = findViewById<Button>(R.id.login_action_btn)
        regText = findViewById<TextView>(R.id.registration_link_text_view)

        mailInput.initType(InputTextComponentType.EMAIL_INPUT)
        passInput.initType(InputTextComponentType.PASS_INPUT)

        setListeners()
    }

    private fun setListeners() {
        loginBtn.setOnClickListener {
            mPresenter.doLogin(mailInput.getText(), passInput.getText())
        }

        regText.setOnClickListener {
            var intent: Intent = Intent(this, RegistrationActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(intent)
        }
    }

    override fun doLoginOK(user: FirebaseUser) {
        var intent: Intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
        startActivity(intent)
    }

    override fun showError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
