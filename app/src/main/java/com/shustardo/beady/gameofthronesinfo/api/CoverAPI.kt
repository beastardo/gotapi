package com.shustardo.beady.gameofthronesinfo.api

class CoverAPI() {
    companion object {
        val ISBN: String = "isbn"
        val SIZE_S: String = "S"
        val SIZE_M: String = "M"
        val SIZE_L: String = "L"
    }

    // BASE COMPLETE URL :  "http://covers.openlibrary.org/b/$key/$value-$size.jpg"
    private val baseURL: String = "http://covers.openlibrary.org/b/"

    fun getBookCover(key: String, value: String, size: String): String {
        return "$baseURL$key/$value-$size.jpg"
    }
}