package com.shustardo.beady.gameofthronesinfo.models

data class GotBookPreviewModel(val title: String, val isbn: String, val url: String)