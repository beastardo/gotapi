package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotHousePreviewModel

interface IHouseFragmentPresenter {
    fun loadData(pageID: String)
    fun returnData(housePreviewModelList: List<GotHousePreviewModel>)
}