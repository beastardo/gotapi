package com.shustardo.beady.gameofthronesinfo.activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.fragments.BooksFragment
import com.shustardo.beady.gameofthronesinfo.fragments.CharacterFragment
import com.shustardo.beady.gameofthronesinfo.fragments.HouseFragment
import com.shustardo.beady.gameofthronesinfo.interfaces.IMainActivityView

class MainActivity : AppCompatActivity(), IMainActivityView {
    private lateinit var mBottomNavigation: BottomNavigationView
    private lateinit var mProgressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Logger.addLogAdapter(AndroidLogAdapter())
        Logger.d("MainActivity")
        setContentView(R.layout.main_activity)
        initView()
        setListeners()
        mBottomNavigation.selectedItemId = R.id.action_books
    }

    private fun initView() {
        mBottomNavigation = findViewById<BottomNavigationView>(R.id.main_bottomnav)
        mProgressBar = findViewById<ProgressBar>(R.id.mainactivity_progressbar)
    }

    private fun setListeners() {
        mBottomNavigation.setOnNavigationItemSelectedListener { item: MenuItem ->

            when (item.itemId) {
                //TODO("This need to be changed for fragments")
                R.id.action_books -> {
                    Toast.makeText(this, "Books", Toast.LENGTH_SHORT).show()

                    val booksFragment = BooksFragment()
                    booksFragment.setArguments(this)
                    val tag = "booksFragment"
                    changeFragment(booksFragment, tag)
                    true
                }

                R.id.action_characters -> {
                    Toast.makeText(this, "Characters", Toast.LENGTH_SHORT).show()

                    val charactersFragment = CharacterFragment()
                    charactersFragment.setArguments(this)
                    val tag = "charactersFragment"
                    changeFragment(charactersFragment, tag)
                    true
                }

                R.id.action_houses -> {
                    Toast.makeText(this, "Houses", Toast.LENGTH_SHORT).show()
                    val housesFragment = HouseFragment.newInstance(this)

                    val tag = "housesFragment"
                    changeFragment(housesFragment, tag)
                    true
                }

                else -> {
                    Toast.makeText(this, "Meeeeck!", Toast.LENGTH_SHORT).show()
                    false
                }
            }
        }
    }

    private fun changeFragment(fragment: Fragment, tag: String) {
        var transaction = getSupportFragmentManager().beginTransaction()
        transaction.replace(R.id.main_container, fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commit()
        showProgress(false)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            mProgressBar.visibility = View.VISIBLE
        } else {
            mProgressBar.visibility = View.GONE
        }
    }
}
