package com.shustardo.beady.gameofthronesinfo.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.adapters.CharacterLinearViewAdapter
import com.shustardo.beady.gameofthronesinfo.components.listeners.EndlessRecyclerViewScrollListener
import com.shustardo.beady.gameofthronesinfo.interfaces.ICharacterFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.interfaces.ICharacterFragmentView
import com.shustardo.beady.gameofthronesinfo.interfaces.IMainActivityView
import com.shustardo.beady.gameofthronesinfo.models.GotCharacterPreviewModel
import com.shustardo.beady.gameofthronesinfo.presenters.CharacterFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils

class CharacterFragment : Fragment(), ICharacterFragmentView {
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener
    private lateinit var presenter: ICharacterFragmentPresenter
    private lateinit var mViewMainActivity: IMainActivityView
    private lateinit var dataList: ArrayList<GotCharacterPreviewModel>
    private lateinit var adapter: CharacterLinearViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_character, container, false)
        val layoutManager: LinearLayoutManager = LinearLayoutManager(context)

        mRecyclerView = view.findViewById(R.id.endlessRecyclerView) as RecyclerView
        presenter = CharacterFragmentPresenter(mViewMainActivity, this)
        dataList = ArrayList<GotCharacterPreviewModel>()
        adapter = CharacterLinearViewAdapter(dataList)

        mRecyclerView.adapter = adapter
        mRecyclerView.layoutManager = layoutManager

        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager, ConstantUtils.ENDLESS_CHAR_THRESHOLD) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                loadNextDataFromApi(page)
            }
        }
        mRecyclerView.addOnScrollListener(scrollListener)

        presenter.loadData(ConstantUtils.API_PAGINATED_FIRST_PAGE)

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        fun newInstance(): CharacterFragment {
            val fragment = CharacterFragment()
            return fragment
        }
    }

    fun setArguments(viewMainActivity: IMainActivityView) {
        mViewMainActivity = viewMainActivity
    }

    private fun loadNextDataFromApi(page: Int) {
        presenter.loadData(page.toString())
    }

    override fun inflateRecyclerView(characterPreviewModelList: List<GotCharacterPreviewModel>) {
        dataList.addAll(characterPreviewModelList)
        refresh()
    }

    override fun refresh() {
        mRecyclerView.adapter.notifyDataSetChanged()
    }
}
