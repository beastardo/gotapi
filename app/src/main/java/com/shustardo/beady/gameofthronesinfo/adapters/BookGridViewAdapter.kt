package com.shustardo.beady.gameofthronesinfo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.api.CoverBookAPIFirebase
import com.shustardo.beady.gameofthronesinfo.fragments.BooksFragment
import com.shustardo.beady.gameofthronesinfo.models.GotBookPreviewModel

class BookGridViewAdapter(val booksList: ArrayList<GotBookPreviewModel>, val booksFragment: BooksFragment) :
        RecyclerView.Adapter<BookGridViewAdapter.ViewHolder>() {
    private val fixedImageSize: Int = 600

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val gotBook: GotBookPreviewModel = booksList[position]
        holder?.title?.text = gotBook.title
        CoverBookAPIFirebase().loadImageByISBNIntoView(gotBook.isbn,holder?.image,booksFragment.context,fixedImageSize)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.book_preview, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return booksList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.book_preview_title) as TextView
        val image = itemView.findViewById(R.id.book_preview_image) as ImageView
    }
}