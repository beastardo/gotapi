package com.shustardo.beady.gameofthronesinfo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.models.GotCharacterPreviewModel
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils

class CharacterLinearViewAdapter(val charList: ArrayList<GotCharacterPreviewModel>) :
        RecyclerView.Adapter<CharacterLinearViewAdapter.ViewHolder>() {
    private lateinit var gotChar: GotCharacterPreviewModel
    private var gotCharName: String = ConstantUtils.EMPTY_STRING
    private lateinit var gotCharAliases: List<String>
    private var gotCharGender: String = ConstantUtils.EMPTY_STRING

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        gotChar = charList[position]
        initBindHolder(holder!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.character_preview, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return charList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val charName = itemView.findViewById(R.id.character_pre_name) as TextView
        val alias = itemView.findViewById(R.id.character_pre_alias) as TextView
        val gender = itemView.findViewById(R.id.character_pre_gender) as TextView
    }

    private fun initBindHolder(holder: ViewHolder) {
        gotCharName = gotChar.name
        gotCharAliases = gotChar.alias as List<String>
        gotCharGender = gotChar.gender
        
        if (gotCharName.equals(ConstantUtils.EMPTY_STRING)) {
            holder.charName.text = "Name unknown"
        } else {
            holder.charName.text = gotCharName
        }

        if (gotCharAliases.isEmpty() || (gotCharAliases.size == 1 && gotCharAliases[0].equals(ConstantUtils.EMPTY_STRING))) {
            holder.alias.text = "Alias unknown"
        } else if (gotCharAliases.size == 1) {
            holder.alias.text = "Aka: ${gotCharAliases[0]}"
        } else {
            holder.alias.text = "Aka: ${gotCharAliases[0]} and ${gotCharAliases.size-1} more"
        }

        holder.gender.text = "Gender: ${gotCharGender}"

    }

    fun getCharName(): String {
        return gotCharName
    }

    fun getCharAliases(): List<String?>? {
        return gotCharAliases
    }

    fun getGender(): String {
        return gotCharGender
    }

}