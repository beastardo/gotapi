package com.shustardo.beady.gameofthronesinfo.interactors

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.shustardo.beady.gameofthronesinfo.interfaces.IRegisterInteractor
import com.shustardo.beady.gameofthronesinfo.presenters.RegisterPresenter
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils

class RegisterInteractor(var presenter: RegisterPresenter) : IRegisterInteractor {
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun doRegistration(email: String, password: String) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task: Task<AuthResult> ->
            if (task.isSuccessful) {
                //Registration OK
                val firebaseUser = this.mAuth.currentUser!!
                presenter.correctLogin(firebaseUser)
            } else {
                //Registration error
                presenter.showError(ConstantUtils.REGISTER_ERROR)
            }
        }
    }
}