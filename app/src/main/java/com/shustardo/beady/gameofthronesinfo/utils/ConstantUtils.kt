package com.shustardo.beady.gameofthronesinfo.utils

/**
 * Created by AndresSalmeron on 19/6/17.
 */
object ConstantUtils {

    //String constants
    const val EMPTY_STRING: String = ""
    const val SPACE_STRING: String = " "

    //Int constants
    const val ZERO_VALUE = 0

    //Registration Constants
    const val REGISTER_ERROR: String = "The user could not be registered"

    //API Constants
    const val API_BASE_URL: String = "http://www.anapioficeandfire.com/api/"
    const val API_ENDPOINT_BOOKS: String = "books"
    const val API_ENDPOINT_CHARS: String = "characters"
    const val API_ENDPOINT_HOUSES: String = "houses"
    const val API_PAGINATED_DEFAULT_RESULTS: String = "15"
    const val API_PAGINATED_FIRST_PAGE: String = "1"
    const val API_PAGINATED_PAGE_SIZE: String = "?pageSize="

    //EndlessScrollListener
    const val ENDLESS_CHAR_THRESHOLD: Int = 5
}