package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotHousePreviewModel

interface IHouseFragmentView {
    fun inflateRecyclerView(housePrevieModel: List<GotHousePreviewModel>)
}