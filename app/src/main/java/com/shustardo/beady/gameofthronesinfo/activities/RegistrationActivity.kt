package com.shustardo.beady.gameofthronesinfo.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseUser
import com.shustardo.beady.gameofthronesinfo.R
import com.shustardo.beady.gameofthronesinfo.components.CustomInputTextComponent
import com.shustardo.beady.gameofthronesinfo.interfaces.IRegisterView
import com.shustardo.beady.gameofthronesinfo.presenters.RegisterPresenter
import com.shustardo.beady.gameofthronesinfo.utils.InputTextComponentType

class RegistrationActivity : AppCompatActivity(), IRegisterView {

    private lateinit var mEmail: CustomInputTextComponent
    private lateinit var mPassword: CustomInputTextComponent
    private lateinit var mCancelRegistration: TextView
    private lateinit var mRegisterButton: Button
    private lateinit var mProgressBar: ProgressBar
    lateinit var presenter: RegisterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_registration_layout)

        presenter = RegisterPresenter(this)

        initView()
        setRegisterView()
        setListeners()
    }

    private fun initView() {
        mEmail = findViewById<CustomInputTextComponent>(R.id.input_user_mail)
        mPassword = findViewById<CustomInputTextComponent>(R.id.input_user_pass)
        mCancelRegistration = findViewById<TextView>(R.id.registration_link_text_view)
        mRegisterButton = findViewById<Button>(R.id.login_action_btn)
        mProgressBar = findViewById<ProgressBar>(R.id.reg_log_progress)


        mEmail.initType(InputTextComponentType.EMAIL_INPUT)
        mPassword.initType(InputTextComponentType.PASS_INPUT)
    }

    private fun setRegisterView() {
        mCancelRegistration.setText(R.string.registration_cancel_registration)
        mRegisterButton.setText(R.string.registration_register)
    }

    private fun setListeners() {
        mRegisterButton.setOnClickListener {
            presenter.doRegistration(mEmail.getText(), mPassword.getText())
        }
        mCancelRegistration.setOnClickListener {
            var intent: Intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(intent)
        }
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun correctRegistration(firebaseUser: FirebaseUser) {
        Toast.makeText(this, firebaseUser.toString(), Toast.LENGTH_SHORT).show()
        var intent: Intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
        startActivity(intent)
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            mProgressBar.visibility = View.VISIBLE
        } else {
            mProgressBar.visibility = View.GONE
        }
    }
}
