package com.shustardo.beady.gameofthronesinfo.interfaces

interface IMainActivityView {
    fun showProgress(show: Boolean)
}