package com.shustardo.beady.gameofthronesinfo.extensions

val MIN_PASSWORD_LENGTH: Int = 6

fun String.validateEmail(): Boolean {
    return !this.isEmpty() && this.contains("@")
}

fun String.validatePassword(): Boolean {
    return !this.isEmpty() && this.length >= MIN_PASSWORD_LENGTH
}

fun String.lastValue(): String {
    if (!this.isEmpty()) {
        val aux = this.split("/")
        return aux[aux.size - 1]
    }
    return this
}