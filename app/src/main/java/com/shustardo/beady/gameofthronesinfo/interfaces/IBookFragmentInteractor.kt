package com.shustardo.beady.gameofthronesinfo.interfaces

interface IBookFragmentInteractor {
    fun loadData(pageId: String)
}