package com.shustardo.beady.gameofthronesinfo.api

import android.content.Context
import android.net.Uri
import android.widget.ImageView
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.shustardo.beady.gameofthronesinfo.R
import com.squareup.picasso.Picasso

class CoverBookAPIFirebase {
    private val storage = FirebaseStorage.getInstance()
    private val storageRef = storage.getReferenceFromUrl("gs://game-of-thrones-66449.appspot.com/")

    fun loadImageByISBNIntoView(isbn: String, image: ImageView?, context: Context, fixedImageSize: Int) {
        val spaceRef = storageRef.child("book/$isbn.jpg")

        spaceRef.downloadUrl.addOnCompleteListener { uri: Task<Uri> ->
            if (uri.isSuccessful) {
                Picasso.with(context)
                        .load(uri.result.toString())
                        .placeholder(R.drawable.placeholder)
                        .resize(fixedImageSize, 0)
                        .into(image)
            }
        }
    }
}