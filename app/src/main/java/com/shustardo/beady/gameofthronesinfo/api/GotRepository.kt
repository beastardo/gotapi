package com.shustardo.beady.gameofthronesinfo.api

import com.shustardo.beady.gameofthronesinfo.interfaces.IGotService
import com.shustardo.beady.gameofthronesinfo.models.GotBookModel
import com.shustardo.beady.gameofthronesinfo.models.GotCharacterModel
import com.shustardo.beady.gameofthronesinfo.models.GotHouseModel
import io.reactivex.Observable

class GotRepository(val apiService: IGotService) {
    fun getBooks(): Observable<List<GotBookModel>> {
        return apiService.searchBookList()
    }

    fun getChars(): Observable<List<GotCharacterModel>> {
        return apiService.searchCharList()
    }

    fun getHouses(): Observable<List<GotHouseModel>> {
        return apiService.searchHouseList()
    }

    fun getBook(bookId: String): Observable<GotBookModel> {
        return apiService.searchBook(bookId)
    }

    fun getChar(charId: String): Observable<GotCharacterModel> {
        return apiService.searchCharacter(charId)
    }

    fun getHouse(houseId: String): Observable<GotHouseModel> {
        return apiService.searchHouse(houseId)
    }

    fun getPaginatedBooks(pageId: String): Observable<List<GotBookModel>> {
        return apiService.searchBooksPaginated(pageId)
    }

    fun getPaginatedChars(pageId: String): Observable<List<GotCharacterModel>> {
        return apiService.searchCharactersPaginated(pageId)
    }

    fun getPaginatedHouses(pageId: String): Observable<List<GotHouseModel>> {
        return apiService.searchHousesPaginated(pageId)
    }
}