package com.shustardo.beady.gameofthronesinfo.api

import com.shustardo.beady.gameofthronesinfo.interfaces.IGotService
import com.shustardo.beady.gameofthronesinfo.models.GotBookModel
import com.shustardo.beady.gameofthronesinfo.models.GotCharacterModel
import com.shustardo.beady.gameofthronesinfo.models.GotHouseModel
import com.shustardo.beady.gameofthronesinfo.utils.ConstantUtils
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GotApi : IGotService {
    private val gotApi: IGotService
    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(ConstantUtils.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        gotApi = retrofit.create(IGotService::class.java)
    }

    override fun searchBookList(): Observable<List<GotBookModel>> {
        return gotApi.searchBookList()
    }

    override fun searchCharList(): Observable<List<GotCharacterModel>> {
        return gotApi.searchCharList()
    }

    override fun searchHouseList(): Observable<List<GotHouseModel>> {
        return gotApi.searchHouseList()
    }

    override fun searchBook(bookId: String): Observable<GotBookModel> {
        return gotApi.searchBook(bookId)
    }

    override fun searchCharacter(charId: String): Observable<GotCharacterModel> {
        return gotApi.searchCharacter(charId)
    }

    override fun searchHouse(houseId: String): Observable<GotHouseModel> {
        return gotApi.searchHouse(houseId)
    }

    override fun searchBooksPaginated(pageId: String): Observable<List<GotBookModel>> {
        return gotApi.searchBooksPaginated(pageId)
    }

    override fun searchCharactersPaginated(pageId: String): Observable<List<GotCharacterModel>> {
        return gotApi.searchCharactersPaginated(pageId)
    }

    override fun searchHousesPaginated(pageId: String): Observable<List<GotHouseModel>> {
        return gotApi.searchHousesPaginated(pageId)
    }
}