package com.shustardo.beady.gameofthronesinfo.interactors

import android.util.Log
import com.shustardo.beady.gameofthronesinfo.api.GotRepository
import com.shustardo.beady.gameofthronesinfo.api.GotRepositoryProvider
import com.shustardo.beady.gameofthronesinfo.interfaces.IBookFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IBookFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.utils.FromApiModelConverterUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class BookFragmentInteractor(var bookFragmentPresenter: IBookFragmentPresenter) : IBookFragmentInteractor {
    private val provider: GotRepository = GotRepositoryProvider.provideGotRepository()

    override fun loadData(pageId: String) {
        provider.getPaginatedBooks(pageId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            bookFragmentPresenter.returnData(FromApiModelConverterUtils().importToListBookPreviewModel(result))
                        },
                        { error ->
                            Log.e("GoT Test", error.message)
                        }
                )
    }
}