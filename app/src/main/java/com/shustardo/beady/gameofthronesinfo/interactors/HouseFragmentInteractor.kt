package com.shustardo.beady.gameofthronesinfo.interactors

import com.orhanobut.logger.Logger
import com.shustardo.beady.gameofthronesinfo.api.GotRepository
import com.shustardo.beady.gameofthronesinfo.api.GotRepositoryProvider
import com.shustardo.beady.gameofthronesinfo.extensions.lastValue
import com.shustardo.beady.gameofthronesinfo.interfaces.IHouseFragmentInteractor
import com.shustardo.beady.gameofthronesinfo.interfaces.IHouseFragmentPresenter
import com.shustardo.beady.gameofthronesinfo.models.GotCharacterModel
import com.shustardo.beady.gameofthronesinfo.utils.FromApiModelConverterUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HouseFragmentInteractor(val presenter: IHouseFragmentPresenter) : IHouseFragmentInteractor {
    private val provider: GotRepository = GotRepositoryProvider.provideGotRepository()

    override fun loadData(pageID: String) {
        provider.getPaginatedHouses(pageID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            presenter.returnData(FromApiModelConverterUtils().importToListHousePreviewModel(result))
                        },
                        { error ->
                            Logger.e("GoT Test" + error.message)
                        }
                )
    }

    override fun recoverCharacter(url: String): Observable<GotCharacterModel> {
        return provider.getChar(url.lastValue())
    }
}