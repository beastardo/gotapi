package com.shustardo.beady.gameofthronesinfo.interfaces

import com.shustardo.beady.gameofthronesinfo.models.GotCharacterModel
import io.reactivex.Observable

interface IHouseFragmentInteractor {
    fun loadData(pageID: String)
    fun recoverCharacter(url: String): Observable<GotCharacterModel>
}