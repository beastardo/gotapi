package com.shustardo.beady.gameofthronesinfo.models

data class GotHousePreviewModel(val name: String, val region: String, var founder: String,
                                var founded: String, val url: String)